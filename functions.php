<?php

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
 
    $parent_style = 'parent-style'; //This is 'twentyfifteen-style' for the Twenty Fifteen theme.
 
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
} 

// plugin loco translate
function catch_responsive_load_theme_textdomain() {
    // Text domaine //  Le chemin dans le theme enfant
    load_theme_textdomain( 'catch-responsive', get_stylesheet_directory() . './assets/languages' );
}
//Hook : after_setup_theme
add_action( 'after_setup_theme', 'catch_responsive_load_theme_textdomain' );

?>
