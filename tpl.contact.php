<?php
/*
Template Name: Contact page
Template Post Type: page
*/

get_header(); ?>

	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php 
			get_template_part( 'template-parts/content', 'page' );
				$adresse = get_field('adresse');
				$url_encoded_address = urlencode($adresse);
?>
			<?php 
				/** 
				 * catchresponsive_comment_section hook
				 *
				 * @hooked catchresponsive_get_comment_section - 10
				 */
				do_action( 'catchresponsive_comment_section' ); 
			?>

		<?php endwhile; // end of the loop. ?>

	</main><!-- #main -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
<iframe src="https://maps.google.com/maps?q=<?php echo $url_encoded_address; ?>&output=embed" height="200" width="300"></iframe>